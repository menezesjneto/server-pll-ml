import tensorflow as tf
from tensorflow.keras.applications.imagenet_utils import decode_predictions
from PIL import Image
import numpy as np
from io import BytesIO

def load_model():
    #[[0.09374183 0.58229613 0.32396197]] INDEX = 0 CLASSE 0, INDEX = 1 CLASSE 1, .... 
    model = tf.keras.models.load_model("keras_model.h5")
    print("Model loaded")
    return model

model = load_model()

def predict(image: Image.Image):
    classes = ['Tela Normal', 'Tela Quebrada']
    image = np.asarray(image.resize((224, 224)))[..., :3]
    image = np.expand_dims(image, 0)
    image = image / 127.5 - 1.0
    print(model.predict(image))
    result = model.predict(image)
    # result = decode_predictions(model.predict(image), 2)
    response = []
    for i, res in enumerate(result):
        for u in range(len(res)):
            resp = {}
            resp["class"] = str(classes[u])
            resp["confidence"] = str(res[u])
            response.append(resp)
    print(response)
    return response

def read_imagefile(file) -> Image.Image:
    image = Image.open(BytesIO(file))
    return image
