import uvicorn
from fastapi import FastAPI, File, UploadFile
import numpy as np
from func import predict, read_imagefile
app = FastAPI()

@app.post("/api/predict/image")
async def predict_api(file: UploadFile = File(...)):
    extension = file.filename.split(".")[-1] in ("jpg", "jpeg", "png")
    if not extension:
        return "Image must be jpg or png format!"
    image = read_imagefile(await file.read())
    prediction = predict(image)
    return prediction

@app.get('/index')
async def hello_world():
    return "hello world"

if __name__ == "__main__":
    uvicorn.run(app, debug=True)